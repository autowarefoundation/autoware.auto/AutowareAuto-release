## Autoware.Auto (dashing) - 0.0.2-1

The packages in the `Autoware.Auto` repository were released into the `dashing` distro by running `/usr/bin/bloom-release Autoware.Auto --rosdistro dashing --edit` on `Mon, 14 Oct 2019 15:20:15 -0000`

These packages were released:
- `autoware_auto_algorithm`
- `autoware_auto_cmake`
- `autoware_auto_create_pkg`
- `autoware_auto_examples`
- `autoware_auto_geometry`
- `autoware_auto_helper_functions`
- `autoware_auto_msgs`
- `euclidean_cluster`
- `euclidean_cluster_nodes`
- `hungarian_assigner`
- `kalman_filter`
- `lidar_utils`
- `localization_common`
- `localization_nodes`
- `motion_model`
- `ndt`
- `optimization`
- `point_cloud_fusion`
- `ray_ground_classifier`
- `ray_ground_classifier_nodes`
- `serial_driver`
- `velodyne_driver`
- `velodyne_node`
- `voxel_grid`
- `voxel_grid_nodes`

These packages were explicitly ignored:
- `autoware_auto_integration_tests`

Version of package(s) in repository `Autoware.Auto`:

- upstream repository: https://gitlab.com/autowarefoundation/autoware.auto/AutowareAuto.git
- release repository: https://gitlab.com/AutowareAuto/AutowareAuto-release.git
- rosdistro version: `0.0.1-1`
- old version: `0.0.1-1`
- new version: `0.0.2-1`

Versions of tools used:

- bloom version: `0.8.0`
- catkin_pkg version: `0.4.13`
- rosdep version: `0.16.1`
- rosdistro version: `0.7.5`
- vcstools version: `0.1.42`


## Autoware.Auto (dashing) - 0.0.1-1

The packages in the `Autoware.Auto` repository were released into the `dashing` distro by running `/usr/bin/bloom-release --rosdistro dashing --track dashing Autoware.Auto --edit` on `Mon, 05 Aug 2019 11:08:34 -0000`

These packages were released:
- `autoware_auto_algorithm`
- `autoware_auto_cmake`
- `autoware_auto_create_pkg`
- `autoware_auto_examples`
- `autoware_auto_geometry`
- `autoware_auto_msgs`
- `euclidean_cluster`
- `euclidean_cluster_nodes`
- `hungarian_assigner`
- `kalman_filter`
- `lidar_utils`
- `motion_model`
- `point_cloud_fusion`
- `ray_ground_classifier`
- `ray_ground_classifier_nodes`
- `udp_driver`
- `velodyne_driver`
- `velodyne_node`
- `voxel_grid`
- `voxel_grid_nodes`

These packages were explicitly ignored:
- `autoware_auto_integration_tests`

Version of package(s) in repository `Autoware.Auto`:

- upstream repository: https://gitlab.com/AutowareAuto/AutowareAuto.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `0.0.1-1`

Versions of tools used:

- bloom version: `0.8.0`
- catkin_pkg version: `0.4.13`
- rosdep version: `0.15.2`
- rosdistro version: `0.7.4`
- vcstools version: `0.1.40`


# AutowareAuto-release

